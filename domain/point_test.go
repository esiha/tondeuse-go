package domain

import (
	"github.com/shoenig/test/must"
	"testing"
)

func TestPoint_Plus(t *testing.T) {
	p1 := NewPoint(1, 2)
	p2 := NewPoint(3, 5)

	must.Eq(t, p1.Plus(p2), NewPoint(p1.X()+p2.X(), p1.Y()+p2.Y()))
}

func TestPoint_IsInBottomLeftQuadrantOf(t *testing.T) {
	ref := NewPoint(2, 4)
	tests := []struct {
		name     string
		other    Point
		expected bool
	}{
		{"same point", ref, true},
		{"bigger x", ref.Plus(NewPoint(1, 0)), true},
		{"bigger y", ref.Plus(NewPoint(0, 1)), true},
		{"smaller x", ref.Plus(NewPoint(-1, 0)), false},
		{"smaller y", ref.Plus(NewPoint(0, -1)), false},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			must.Eq(t, ref.IsInBottomLeftQuadrantOf(test.other), test.expected)
		})
	}
}

func TestPoint_IsInTopRightQuadrantOf(t *testing.T) {
	ref := NewPoint(2, 4)
	tests := []struct {
		name     string
		other    Point
		expected bool
	}{
		{"same point", ref, true},
		{"bigger x", ref.Plus(NewPoint(1, 0)), false},
		{"bigger y", ref.Plus(NewPoint(0, 1)), false},
		{"smaller x", ref.Plus(NewPoint(-1, 0)), true},
		{"smaller y", ref.Plus(NewPoint(0, -1)), true},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			must.Eq(t, ref.IsInTopRightQuadrantOf(test.other), test.expected)
		})
	}
}
