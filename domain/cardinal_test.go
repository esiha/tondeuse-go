package domain

import (
	"github.com/shoenig/test/must"
	"testing"
)

func TestCardinal_Rotate90DegreesLeft(t *testing.T) {
	tests := []struct {
		name     string
		from     Cardinal
		expected Cardinal
	}{
		{"North → West", North, West},
		{"West → South", West, South},
		{"South → East", South, East},
		{"East → North", East, North},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			must.Eq(t, test.from.Rotate90DegreesLeft(), test.expected)
		})
	}
}

func TestCardinal_Rotate90DegreesRight(t *testing.T) {
	tests := []struct {
		name     string
		from     Cardinal
		expected Cardinal
	}{
		{"North → East", North, East},
		{"East → South", East, South},
		{"South → West", South, West},
		{"West → North", West, North},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			must.Eq(t, test.from.Rotate90DegreesRight(), test.expected)
		})
	}
}

func TestCardinal_AsVector(t *testing.T) {
	tests := []struct {
		name     string
		from     Cardinal
		expected Point
	}{
		{"North → (0, 1)", North, NewPoint(0, 1)},
		{"East → (1, 0)", East, NewPoint(1, 0)},
		{"South → (-1, 0)", South, NewPoint(0, -1)},
		{"West → (0, -1)", West, NewPoint(-1, 0)},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			must.Eq(t, test.from.AsVector(), test.expected)
		})
	}
}
