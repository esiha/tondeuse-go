package domain

import (
	"github.com/shoenig/test/must"
	"testing"
)

func TestPosition_Rotate90DegreesLeft(t *testing.T) {
	point := NewPoint(1, 2)
	orientation := North
	start := NewPosition(point, orientation)

	actual := start.Rotate90DegreesLeft()

	must.Eq(t, actual.Point(), start.Point())
	must.Eq(t, actual.Orientation(), start.Orientation().Rotate90DegreesLeft())
}

func TestPosition_Rotate90DegreesRight(t *testing.T) {
	point := NewPoint(1, 2)
	orientation := North
	start := NewPosition(point, orientation)

	actual := start.Rotate90DegreesRight()

	must.Eq(t, actual.Point(), start.Point())
	must.Eq(t, actual.Orientation(), start.Orientation().Rotate90DegreesRight())
}

func TestPosition_TranslateOnce(t *testing.T) {
	point := NewPoint(4, 5)
	orientation := East
	start := NewPosition(point, orientation)

	actual := start.TranslateOnce()

	must.Eq(t, actual.Point(), start.Point().Plus(orientation.AsVector()))
	must.Eq(t, actual.Orientation(), start.Orientation())
}
