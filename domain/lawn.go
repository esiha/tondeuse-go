package domain

import "fmt"

type RectangularLawn struct {
	bottomLeftCorner Point
	topRightCorner   Point
}

var origin = NewPoint(0, 0)

func NewRectangularLawn(topRightCorner Point) (RectangularLawn, error) {
	if topRightCorner.IsInBottomLeftQuadrantOf(origin) {
		return RectangularLawn{}, fmt.Errorf("topRightCorner must be in top right quadrant of origin (0,0), got: %v", topRightCorner)
	}
	return RectangularLawn{origin, topRightCorner}, nil
}

func (r RectangularLawn) Contains(point Point) bool {
	return point.IsInTopRightQuadrantOf(r.bottomLeftCorner) &&
		point.IsInBottomLeftQuadrantOf(r.topRightCorner)
}
