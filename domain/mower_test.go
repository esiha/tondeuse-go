package domain

import (
	"github.com/shoenig/test/must"
	"testing"
)

func TestMower_Run(t *testing.T) {
	lawn, _ := NewRectangularLawn(NewPoint(5, 5))
	startPosition := NewPosition(NewPoint(3, 3), East)
	tests := []struct {
		name             string
		instructions     []Instruction
		expectedPosition Position
	}{
		{
			"all instructions",
			[]Instruction{TurnLeft, MoveOnce, TurnRight, TurnRight},
			NewPosition(NewPoint(3, 4), South),
		},
		{
			"remaining inside Lawn",
			[]Instruction{MoveOnce, MoveOnce, MoveOnce, TurnRight},
			NewPosition(NewPoint(5, 3), South),
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			mower := NewMower(startPosition, test.instructions)
			must.Nil(t, mower.Run(lawn))
			must.Eq(t, mower.Position(), test.expectedPosition)
		})
	}
}

func TestMower_Run_ErrorsWhenStartingOutsideLawn(t *testing.T) {
	lawn, _ := NewRectangularLawn(NewPoint(4, 4))
	startPosition := NewPosition(NewPoint(5, 5), East)
	mower := NewMower(startPosition, []Instruction{MoveOnce})

	must.NotNil(t, mower.Run(lawn))
}
