package domain

import "fmt"

type Cardinal uint8

const (
	North Cardinal = iota
	East
	South
	West
)

func (c Cardinal) Rotate90DegreesLeft() Cardinal {
	switch c {
	case North:
		return West
	case West:
		return South
	case South:
		return East
	case East:
		return North
	default:
		panic(fmt.Sprintf("unknown cardinal: %v", c))
	}
}

func (c Cardinal) Rotate90DegreesRight() Cardinal {
	switch c {
	case North:
		return East
	case East:
		return South
	case South:
		return West
	case West:
		return North
	default:
		panic(fmt.Sprintf("unknown cardinal: %v", c))
	}
}

func (c Cardinal) AsVector() Point {
	switch c {
	case North:
		return NewPoint(0, 1)
	case East:
		return NewPoint(1, 0)
	case South:
		return NewPoint(0, -1)
	case West:
		return NewPoint(-1, 0)
	default:
		panic(fmt.Sprintf("unknown cardinal: %v", c))
	}
}
