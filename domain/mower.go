package domain

import "fmt"

type Mower struct {
	currentPosition Position
	instructions    []Instruction
}

func NewMower(startPosition Position, instructions []Instruction) Mower {
	return Mower{startPosition, instructions}
}

func (m *Mower) Run(lawn RectangularLawn) error {
	if err := m.requireCurrentPositionInside(lawn); err != nil {
		return err
	}
	m.run(lawn)
	return nil
}

func (m *Mower) requireCurrentPositionInside(lawn RectangularLawn) error {
	if !lawn.Contains(m.currentPosition.Point()) {
		return fmt.Errorf("mower starts outside lawn (%v): %v", lawn, m.currentPosition)
	}
	return nil
}

func (m *Mower) run(lawn RectangularLawn) {
	for _, instruction := range m.instructions {
		if newPosition := instruction.Execute(m.currentPosition); lawn.Contains(newPosition.Point()) {
			m.currentPosition = newPosition
		}
	}
}

func (m *Mower) Position() Position {
	return m.currentPosition
}
