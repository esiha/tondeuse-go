package domain

type Position struct {
	point       Point
	orientation Cardinal
}

func NewPosition(point Point, orientation Cardinal) Position {
	return Position{point, orientation}
}

func (p Position) Rotate90DegreesLeft() Position {
	return Position{p.point, p.orientation.Rotate90DegreesLeft()}
}

func (p Position) Rotate90DegreesRight() Position {
	return Position{p.point, p.orientation.Rotate90DegreesRight()}
}

func (p Position) TranslateOnce() Position {
	return Position{p.point.Plus(p.orientation.AsVector()), p.orientation}
}

func (p Position) Point() Point {
	return p.point
}

func (p Position) Orientation() Cardinal {
	return p.orientation
}
