package domain

type Point struct {
	x int
	y int
}

func NewPoint(x int, y int) Point {
	return Point{x, y}
}

func (p Point) Plus(other Point) Point {
	return Point{p.x + other.x, p.y + other.y}
}

func (p Point) IsInBottomLeftQuadrantOf(other Point) bool {
	return p.x <= other.x && p.y <= other.y
}

func (p Point) IsInTopRightQuadrantOf(other Point) bool {
	return p.x >= other.x && p.y >= other.y
}

func (p Point) X() int {
	return p.x
}

func (p Point) Y() int {
	return p.y
}
