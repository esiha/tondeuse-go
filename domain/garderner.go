package domain

type Gardener struct {
	lawn   RectangularLawn
	mowers []Mower
}

func NewGardener(lawn RectangularLawn, mowers []Mower) Gardener {
	return Gardener{lawn, mowers}
}

func (g *Gardener) Run() error {
	for i := range g.mowers {
		if err := g.mowers[i].Run(g.lawn); err != nil {
			return err
		}
	}
	return nil
}

func (g *Gardener) MowersPositions() []Position {
	positions := make([]Position, len(g.mowers))
	for i, mower := range g.mowers {
		positions[i] = mower.currentPosition
	}
	return positions
}
