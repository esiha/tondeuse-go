package domain

import (
	"github.com/shoenig/test/must"
	"testing"
)

func TestInstruction_Execute(t *testing.T) {
	start := NewPosition(NewPoint(3, 3), West)
	tests := []struct {
		name        string
		instruction Instruction
		expected    Position
	}{
		{"MoveOnce → Translate position", MoveOnce, start.TranslateOnce()},
		{"TurnLeft → Rotate 90° left", TurnLeft, start.Rotate90DegreesLeft()},
		{"TurnRight → Rotate 90° right", TurnRight, start.Rotate90DegreesRight()},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			must.Eq(t, test.instruction.Execute(start), test.expected)
		})
	}
}
