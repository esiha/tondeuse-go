package domain

import (
	"github.com/shoenig/test/must"
	"testing"
)

func TestGardener_Run(t *testing.T) {
	lawn, _ := NewRectangularLawn(NewPoint(5, 5))
	gardener := NewGardener(
		lawn,
		[]Mower{
			NewMower(
				NewPosition(NewPoint(1, 2), North),
				[]Instruction{TurnLeft, MoveOnce, TurnLeft, MoveOnce, TurnLeft, MoveOnce, TurnLeft, MoveOnce, MoveOnce},
			),
			NewMower(
				NewPosition(NewPoint(3, 3), East),
				[]Instruction{MoveOnce, MoveOnce, TurnRight, MoveOnce, MoveOnce, TurnRight, MoveOnce, TurnRight, TurnRight, MoveOnce},
			),
		},
	)

	must.Nil(t, gardener.Run())

	must.Eq(t,
		gardener.MowersPositions(),
		[]Position{NewPosition(NewPoint(1, 3), North), NewPosition(NewPoint(5, 1), East)},
	)
}

func TestGardener_Run_errorsWhenMowerErrors(t *testing.T) {
	lawn, _ := NewRectangularLawn(NewPoint(5, 5))
	gardener := NewGardener(lawn, []Mower{NewMower(NewPosition(NewPoint(6, 6), North), []Instruction{MoveOnce})})

	must.NotNil(t, gardener.Run())
}
