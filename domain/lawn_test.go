package domain

import (
	"github.com/shoenig/test/must"
	"testing"
)

func TestNewRectangularLawn(t *testing.T) {
	tests := []struct {
		name           string
		topRightCorner Point
		wantError      bool
	}{
		{"top right corner below origin", NewPoint(-1, -1), true},
		{"top right corner above origin", NewPoint(1, 1), false},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			_, err := NewRectangularLawn(test.topRightCorner)
			must.Eq(t, err != nil, test.wantError)
		})
	}
}

func TestRectangularLawn_Contains(t *testing.T) {
	topRightCorner := NewPoint(3, 4)
	lawn, _ := NewRectangularLawn(topRightCorner)
	tests := []struct {
		name     string
		point    Point
		expected bool
	}{
		{"origin", NewPoint(0, 0), true},
		{"top right corner", topRightCorner, true},
		{"inside", topRightCorner.Plus(NewPoint(-1, -1)), true},
		{"bottom left quadrant of origin", NewPoint(-1, -1), false},
		{"top right quadrant of top right corner", topRightCorner.Plus(NewPoint(1, 1)), false},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			must.Eq(t, lawn.Contains(test.point), test.expected)
		})
	}
}
