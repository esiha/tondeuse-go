package domain

import "fmt"

type Instruction uint8

const (
	MoveOnce Instruction = iota
	TurnLeft
	TurnRight
)

func (i Instruction) Execute(position Position) Position {
	switch i {
	case MoveOnce:
		return position.TranslateOnce()
	case TurnLeft:
		return position.Rotate90DegreesLeft()
	case TurnRight:
		return position.Rotate90DegreesRight()
	default:
		panic(fmt.Sprintf("unknown instruction: %v", i))
	}
}
