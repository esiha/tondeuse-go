package main

import (
	"github.com/shoenig/test/must"
	"io"
	"os"
	"testing"
)

func Test_validConfig(t *testing.T) {
	out, err := captureStdout(func() { run([]string{"cli", "./valid_config"}) })

	must.Nil(t, err)
	must.Eq(t, out, []byte("1 3 N\n5 1 E\n"))
}

func captureStdout(f func()) ([]byte, error) {
	if r, w, err := os.Pipe(); err != nil {
		panic(err)
	} else {
		oldStdout := os.Stdout
		os.Stdout = w
		defer func() { os.Stdout = oldStdout }()

		f()

		if err := w.Close(); err != nil {
			panic(err)
		}
		return io.ReadAll(r)
	}
}
