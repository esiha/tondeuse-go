package main

import (
	"fmt"
	"os"
	"tondeuse/config"
	"tondeuse/domain"
)

func main() {
	os.Exit(run(os.Args))
}

func run(args []string) int {
	if len(args) != 2 {
		_, _ = fmt.Fprintf(os.Stderr, "Usage: %v <path-to-config>\n", args[0])
		return 1
	} else if cfg, err := config.ParseConfigFile(args[1]); err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Error: %v\n", err)
		return 2
	} else if gardener, err := cfg.ToGardener(); err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Error: %v\n", err)
		return 3
	} else if err := gardener.Run(); err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Error: %v\n", err)
		return 4
	} else {
		printPositions(gardener.MowersPositions())
		return 0
	}
}

func printPositions(positions []domain.Position) {
	for _, position := range positions {
		point := position.Point()
		fmt.Printf("%v %v %v\n",
			point.X(),
			point.Y(),
			toNEWS(position.Orientation()),
		)
	}
}

func toNEWS(cardinal domain.Cardinal) string {
	switch cardinal {
	case domain.North:
		return "N"
	case domain.East:
		return "E"
	case domain.South:
		return "S"
	case domain.West:
		return "W"
	default:
		panic(fmt.Errorf("unknown cardinal: %v", cardinal))
	}
}
