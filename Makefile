.PHONY: test
test: build
	go test ./...

.PHONY: build
build:
	go build ./...

.PHONY: demo
demo: build
	go run ./... valid_config