package config

import (
	"github.com/shoenig/test/must"
	"os"
	"testing"
	. "tondeuse/domain"
)

func TestParseConfigFile_valid(t *testing.T) {
	path := tempFileContaining(
		"4 5",
		"1 2 N",
		"GAGAGAGAA",
		"3 3 E",
		"AADAADADDA",
		"2 2 W",
		"DDDD",
		"1 1 S",
		"AAA",
	)

	config, err := ParseConfigFile(path)

	must.Nil(t, err)

	must.Eq(t, config, Config{
		NewPoint(4, 5),
		[]MowerConfig{{
			NewPosition(NewPoint(1, 2), North),
			[]Instruction{TurnLeft, MoveOnce, TurnLeft, MoveOnce, TurnLeft, MoveOnce, TurnLeft, MoveOnce, MoveOnce},
		}, {
			NewPosition(NewPoint(3, 3), East),
			[]Instruction{MoveOnce, MoveOnce, TurnRight, MoveOnce, MoveOnce, TurnRight, MoveOnce, TurnRight, TurnRight, MoveOnce},
		}, {
			NewPosition(NewPoint(2, 2), West),
			[]Instruction{TurnRight, TurnRight, TurnRight, TurnRight},
		}, {
			NewPosition(NewPoint(1, 1), South),
			[]Instruction{MoveOnce, MoveOnce, MoveOnce},
		}},
	})
}

func TestParseConfigFile_invalid(t *testing.T) {
	tests := []struct {
		name  string
		lines []string
	}{
		{
			"empty file",
			[]string{},
		}, {
			"missing Y in top right corner",
			[]string{
				"4",
				"1 2 N",
				"GA",
			},
		}, {
			"spurious value in top right corner",
			[]string{
				"4 5 1",
				"1 2 N",
				"GA",
			},
		}, {
			"invalid number in top right corner X",
			[]string{
				"A 5",
				"1 2 N",
				"GA",
			},
		}, {
			"invalid number in top right corner Y",
			[]string{
				"4 B",
				"1 2 N",
				"GA",
			},
		}, {
			"missing mower orientation",
			[]string{
				"4 5",
				"1 2",
				"GA",
			},
		}, {
			"spurious value in mower orientation",
			[]string{
				"4 5",
				"1 2 N N",
				"GA",
			},
		}, {
			"invalid mower orientation",
			[]string{
				"4 5",
				"1 2 A",
				"GA",
			},
		}, {
			"invalid number in mower position X",
			[]string{
				"4 5",
				"A 2 N",
				"GA",
			},
		}, {
			"invalid number in mower position Y",
			[]string{
				"4 5",
				"1 B N",
				"GA",
			},
		}, {
			"missing mower instructions",
			[]string{
				"4 5",
				"1 2 N",
			},
		}, {
			"invalid mower instruction",
			[]string{
				"4 5",
				"1 2 N",
				"AXG",
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			path := tempFileContaining(test.lines...)

			_, err := ParseConfigFile(path)

			must.NotNil(t, err)
		})
	}
}

func tempFileContaining(lines ...string) string {
	if file, err := os.CreateTemp("", "tmp"); err != nil {
		panic(err)
	} else {
		defer func() { _ = file.Close() }()
		for _, line := range lines {
			if _, err := file.Write([]byte(line + "\n")); err != nil {
				panic(err)
			}
		}

		return file.Name()
	}
}
