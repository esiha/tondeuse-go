package config

import (
	"github.com/shoenig/test/must"
	"testing"
	. "tondeuse/domain"
)

func TestConfig_ToGardener(t *testing.T) {
	lawn, _ := NewRectangularLawn(NewPoint(4, 5))
	config := Config{
		NewPoint(4, 5),
		[]MowerConfig{{
			NewPosition(NewPoint(2, 2), West),
			[]Instruction{TurnRight, TurnRight, TurnRight, TurnRight},
		}, {
			NewPosition(NewPoint(1, 1), South),
			[]Instruction{MoveOnce, MoveOnce, MoveOnce},
		}},
	}

	gardener, err := config.ToGardener()

	must.Nil(t, err)
	must.Eq(t, gardener, NewGardener(
		lawn,
		[]Mower{
			NewMower(config.mowersConfig[0].startPosition, config.mowersConfig[0].instructions),
			NewMower(config.mowersConfig[1].startPosition, config.mowersConfig[1].instructions),
		},
	))
}

func TestConfig_ToGardener_errorsWithInvalidLawn(t *testing.T) {
	config := Config{
		NewPoint(-1, -1),
		[]MowerConfig{{
			NewPosition(NewPoint(1, 1), South),
			[]Instruction{MoveOnce, MoveOnce, MoveOnce},
		}},
	}

	_, err := config.ToGardener()

	must.NotNil(t, err)
}
