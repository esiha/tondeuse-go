package config

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"tondeuse/domain"
)

func ParseConfigFile(path string) (Config, error) {
	if lines, err := readLines(path); err != nil {
		return Config{}, err
	} else {
		return parseConfig(lines)
	}
}

func readLines(path string) ([]string, error) {
	if file, err := os.Open(path); err != nil {
		return nil, err
	} else {
		defer func() { _ = file.Close() }()
		lines := make([]string, 0)
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			lines = append(lines, scanner.Text())
		}
		return lines, nil
	}
}

const (
	topRightCornerLine = iota
	mowersConfigStart
)

func parseConfig(lines []string) (Config, error) {
	if isEvenSized(lines) {
		return Config{}, fmt.Errorf("expected an odd number of lines, got: %v", len(lines))
	}
	if topRightCorner, err := parseTopRightCorner(lines[topRightCornerLine]); err != nil {
		return Config{}, err
	} else if mowersConfig, err := parseMowersConfig(lines[mowersConfigStart:]); err != nil {
		return Config{}, err
	} else {
		return Config{topRightCorner, mowersConfig}, nil
	}
}

func isEvenSized(lines []string) bool {
	return len(lines)%2 == 0
}

const (
	pointX = iota
	pointY
	nPartsPoint
)

func parseTopRightCorner(line string) (domain.Point, error) {
	if parts := strings.Split(line, " "); len(parts) != nPartsPoint {
		return domain.Point{}, fmt.Errorf("expected %v parts in top right corner, got: %v", nPartsPoint, len(parts))
	} else if x, err := parseInt(parts[pointX]); err != nil {
		return domain.Point{}, err
	} else if y, err := parseInt(parts[pointY]); err != nil {
		return domain.Point{}, err
	} else {
		return domain.NewPoint(x, y), nil
	}
}

func parseInt(input string) (int, error) {
	i, err := strconv.ParseInt(input, 10, 32)
	return int(i), err
}

const (
	positionLine = iota
	instructionsLine
	nLinesPerMower
)

func parseMowersConfig(lines []string) ([]MowerConfig, error) {
	mowersConfig := make([]MowerConfig, 0)
	for i := 0; i < len(lines); i += nLinesPerMower {
		if mowerConfig, err := parseMowerConfig(lines[i : i+nLinesPerMower]); err != nil {
			return []MowerConfig{}, err
		} else {
			mowersConfig = append(mowersConfig, mowerConfig)
		}
	}
	return mowersConfig, nil
}

func parseMowerConfig(lines []string) (MowerConfig, error) {
	if position, err := parsePosition(lines[positionLine]); err != nil {
		return MowerConfig{}, err
	} else if instructions, err := parseInstructions(lines[instructionsLine]); err != nil {
		return MowerConfig{}, err
	} else {
		return MowerConfig{position, instructions}, nil
	}
}

const (
	positionX = iota
	positionY
	positionOrientation
	nPartsPosition
)

func parsePosition(line string) (domain.Position, error) {
	if parts := strings.Split(line, " "); len(parts) != nPartsPosition {
		return domain.Position{}, fmt.Errorf("expected %v parts in mower position, got: %v", nPartsPosition, len(parts))
	} else if x, err := parseInt(parts[positionX]); err != nil {
		return domain.Position{}, err
	} else if y, err := parseInt(parts[positionY]); err != nil {
		return domain.Position{}, err
	} else if orientation, err := parseCardinal(parts[positionOrientation]); err != nil {
		return domain.Position{}, err
	} else {
		return domain.NewPosition(domain.NewPoint(x, y), orientation), nil
	}
}

func parseCardinal(input string) (domain.Cardinal, error) {
	var c domain.Cardinal
	switch input {
	case "N":
		c = domain.North
	case "E":
		c = domain.East
	case "S":
		c = domain.South
	case "W":
		c = domain.West
	default:
		return 0, fmt.Errorf("invalid Cardinal litteral: %v", input)
	}
	return c, nil
}

func parseInstructions(line string) ([]domain.Instruction, error) {
	instructions := make([]domain.Instruction, 0)
	for _, part := range strings.Split(line, "") {
		if instruction, err := parseInstruction(part); err != nil {
			return nil, err
		} else {
			instructions = append(instructions, instruction)
		}
	}
	return instructions, nil
}

func parseInstruction(input string) (domain.Instruction, error) {
	var i domain.Instruction
	switch input {
	case "D":
		i = domain.TurnRight
	case "G":
		i = domain.TurnLeft
	case "A":
		i = domain.MoveOnce
	default:
		return 0, fmt.Errorf("invalid instruction litteral: %v", input)
	}
	return i, nil
}
