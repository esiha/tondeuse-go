package config

import "tondeuse/domain"

type Config struct {
	topRightCorner domain.Point
	mowersConfig   MowersConfig
}

func (c *Config) ToGardener() (domain.Gardener, error) {
	if lawn, err := domain.NewRectangularLawn(c.topRightCorner); err != nil {
		return domain.Gardener{}, err
	} else {
		return domain.NewGardener(lawn, c.mowersConfig.toMowers()), nil
	}
}

type MowerConfig struct {
	startPosition domain.Position
	instructions  []domain.Instruction
}

type MowersConfig []MowerConfig

func (mcs MowersConfig) toMowers() []domain.Mower {
	mowers := make([]domain.Mower, 0)
	for _, mowerConfig := range mcs {
		mowers = append(mowers, domain.NewMower(mowerConfig.startPosition, mowerConfig.instructions))
	}
	return mowers
}
